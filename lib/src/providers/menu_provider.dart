import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

class _ProviderMenu {
  List<dynamic> options = [];

  _ProviderMenu() {
    // uploadData();
  }

  Future<List<dynamic>> uploadData() async {
    final response = await rootBundle.loadString('data/menu-opts.json');
    Map dataMap = json.decode(response);
    //print(dataMap['rutas']);
    options = dataMap['rutas'];
    return options;
  }
}

final menuProvider = new _ProviderMenu();
