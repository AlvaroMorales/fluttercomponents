import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: [
          Container(
            padding: EdgeInsets.all(6),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://static1.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg"),
//              radius: 1,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 8),
            child: CircleAvatar(
              child: Text("Avt"),
              backgroundColor: Colors.brown,
            ),
          ),
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://wallpapercave.com/wp/wp2529526.jpg'),
          placeholder: AssetImage('assets/073 jar-loading.gif'),
          fadeInDuration: Duration(milliseconds: 200),
        ),
      ),
    );
  }
}
