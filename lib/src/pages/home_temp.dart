import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePagesTemp extends StatelessWidget {
  final options = ['One', 'Two', 'Three', 'Four', 'Five'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Components temps'),
      ),
      body: ListView(
          //children: _createItems()
          children: _createItemsShort()),
    );
  }

  List<Widget> _createItems() {
    List<Widget> lists = new List<Widget>();

    for (String item in options) {
      final tempWidget = ListTile(
        title: Text(item),
      );
      lists..add(tempWidget)..add(Divider());
    }
    return lists;
  }

  List<Widget> _createItemsShort() {
    return options.map((item) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('Este man es el sujeto'),
            leading: Icon(Icons.account_balance_wallet),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {},
          ),
          Divider(),
        ],
      );
    }).toList();
  }
}
