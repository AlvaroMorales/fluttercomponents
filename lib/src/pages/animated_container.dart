import 'dart:math';

import 'package:flutter/material.dart';

class AnimmatedContainerPage extends StatefulWidget {
  @override
  _AnimmatedContainerPageState createState() => _AnimmatedContainerPageState();
}

class _AnimmatedContainerPageState extends State<AnimmatedContainerPage> {
  double _width = 50;
  double _height = 50;
  Color _color = Colors.purple;

  BorderRadiusGeometry _borderRadiosGeometry = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animated Container"),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(seconds: 1),
          curve: Curves.decelerate,
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _borderRadiosGeometry,
            color: _color,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: changeColor),
    );
  }

  void changeColor() {
    final random = Random();
    setState(() {
      _width = random.nextInt(300).toDouble();
      _height = random.nextInt(300).toDouble();
      _color = Color.fromRGBO(
          random.nextInt(255), random.nextInt(255), random.nextInt(255), 1);
      _borderRadiosGeometry =
          BorderRadius.circular(random.nextInt(300).toDouble());
    });
  }
}
