import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _name = "", _email = "", _password = "", _date = "";
  TextEditingController _inputFieldDateController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("It's my input"),
      ),
      body: ListView(
        children: <Widget>[
          _createInput(),
          Divider(),
          _createEmail(),
          Divider(),
          _createPassword(),
          Divider(),
          _createDate(context),
          Divider(),
          _createPerson(),
        ],
      ),
    );
  }

  Widget _createInput() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextField(
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
            counter: Text("Letters ${_name.length}"),
            hintText: "Write your names...",
            labelText: "Name",
            helperText: "Just write your name.",
            suffixIcon: Icon(Icons.accessibility),
            icon: Icon(Icons.account_circle)),
        onChanged: (value) {
          setState(() {
            _name = value;
          });
        },
      ),
    );
  }

  Widget _createEmail() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          hintText: "Write your email...",
          counter: Text("Email ${_email.length}"),
          labelText: "Email",
          helperText: "Just write your email.",
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email),
        ),
        onChanged: (emailValue) => setState(() {
          _email = emailValue;
        }),
      ),
    );
  }

  Widget _createPassword() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextField(
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          hintText: "Write your password...",
          counter: Text("Password ${_password.length}"),
          labelText: "Password",
          helperText: "Just write your password",
          suffixIcon: Icon(Icons.lock),
          icon: Icon(Icons.lock_outline),
        ),
        onChanged: (passwordValue) => setState(() {
          _password = passwordValue;
        }),
      ),
    );
  }

  Widget _createDate(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: "Write the date...",
        labelText: "Date",
        suffixIcon: Icon(Icons.calendar_today),
        icon: Icon(Icons.date_range),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2018),
      lastDate: new DateTime(2025),
      locale: Locale('es', 'ES'),
    );
    if (picked != null) {
      setState(() {
        _date = picked.toString();
        _inputFieldDateController.text = _date;
      });
    }
  }

  Widget _createPerson() {
    return ListTile(
      title: Text("Your name is $_name"),
      subtitle: Text("Email: $_email"),
    );
  }
}
